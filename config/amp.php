<?php

return [

    "debug" => true,
    "handlers" => [
        \LaravelAMP\Handlers\Image::class,
        \LaravelAMP\Handlers\Iframe::class,
        \LaravelAMP\Handlers\Initialize::class,
        \LaravelAMP\Handlers\Form::class,
        \LaravelAMP\Handlers\Links::class,
        \LaravelAMP\Handlers\Menu::class,
        \LaravelAMP\Handlers\Attributes::class,
        \LaravelAMP\Handlers\RemoveNonUsedHtml::class,
    ],

    "enable_for_all_domains" => false,

    // Enable AMP for next domains
    "domains" => []
];
