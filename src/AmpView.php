<?php

namespace LaravelAMP;

use Illuminate\Contracts\View\View;
use LaravelAMP\Services\AmpService;
use LaravelAMP\Services\RenderService;

class AmpView
{
    const HTML = "html";
    const AMP = "amp";

    private $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function render()
    {
        $result = $this->view->render();

        if(AmpService::isAmp()){
            $result = (new RenderService())->render($result);
        }

        return $result;
    }

    public static function make($view, $data){

        $view = \Illuminate\Support\Facades\View::make($view, $data);

        return new AmpView($view);
    }
}
