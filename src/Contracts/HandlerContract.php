<?php


namespace LaravelAMP\Contracts;


interface HandlerContract
{
    public function enabled():bool;
    public function handle(string $html):string;
}
