<?php

namespace LaravelAMP\Handlers;

use LaravelAMP\Contracts\HandlerContract;

class Attributes extends Base implements HandlerContract
{
    public function handle(string $html): string
    {
        $html = preg_replace('#data-amp-custom-#', "", $html);

        return $html;
    }
}
