<?php

namespace LaravelAMP\Handlers;

class Base
{
    public function enabled(): bool{
        return true;
    }

    protected function includeScript($html, $script){
        return preg_replace('#<\/head>#', "\t{$script}\n</head>", $html);
    }
}
