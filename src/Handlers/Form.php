<?php

namespace LaravelAMP\Handlers;

use LaravelAMP\Contracts\HandlerContract;

class Form extends Base implements HandlerContract
{
    private $ampForm = "<script async custom-element=\"amp-form\" src=\"https://cdn.ampproject.org/v0/amp-form-0.1.js\"></script>";

    public function handle(string $html): string
    {
        /*$html = preg_replace('#<form(.*?)>#', "<amp-form$1>", $html);
        $html = preg_replace('#<\/form>#', "</amp-form>", $html);

        $html = preg_replace('#<amp-form#', "<div class='amp-form-container'><amp-form", $html);
        $html = preg_replace('#<\/amp-form>#', "</amp-form></div>", $html);

        if(preg_match('#amp-form#', $html)){
            $html = $this->includeScript($html, $this->ampForm);
        }*/

        if(preg_match('#form#', $html)){
            $html = $this->includeScript($html, $this->ampForm);
        }

        return $html;
    }
}
