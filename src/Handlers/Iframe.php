<?php


namespace LaravelAMP\Handlers;


use App\Helpers\LogHelper;
use LaravelAMP\Contracts\HandlerContract;

class Iframe extends Base implements HandlerContract
{
    public function handle(string $html): string
    {
        $html = preg_replace('#<iframe#', "<amp-iframe", $html);
        $html = preg_replace('#<\/iframe>#', "</amp-iframe>", $html);
        $html = preg_replace('#<amp-iframe(.*?)width="100%"#s', "<amp-iframe$1width=\"700\"", $html);
        $html = preg_replace('#<amp-iframe(.*?)height="100%"#s', "<amp-iframe$1height=\"300\"", $html);
        $html = preg_replace('#<amp-iframe(.*?)src=""#s', "<amp-iframe$1", $html);
        $html = preg_replace('#<amp-iframe(.*?)data-lazy-src=#s', "<amp-iframe$1src=", $html);

        $html = preg_replace_callback('#<amp-iframe(.*?)src="(.*?)"(.*?)><\/amp-iframe>#s', function ($matches){

            if(isset($matches[2]) && preg_match('#youtube#', $matches[2])){

                $width = 700;
                $height = 300;
                $videoID = 0;

                if(preg_match('#<amp-iframe(.*?)src="(.*?)"(.*?)>#s', $matches[0], $subMatches)){

                    if(isset($subMatches[2])){

                        $array = explode("/", $subMatches[2]);

                        $videoID = end($array);
                    }
                }

                if(preg_match('#<amp-iframe(.*?)width="(.*?)"(.*?)>#s', $matches[0], $subMatches)){

                    if(isset($subMatches[2])){
                        $width = $subMatches[2];
                    }
                }

                if(preg_match('#<amp-iframe(.*?)height="(.*?)"(.*?)>#s', $matches[0], $subMatches)){

                    if(isset($subMatches[2])){
                        $height = $subMatches[2];
                    }
                }

                return "<div class='amp-youtube-container'><amp-youtube width=\"{$width}\" height=\"{$height}\" layout=\"responsive\" data-videoid=\"{$videoID}\"></amp-youtube></div>";
            }

            return $matches[0];

        }, $html);

        $html = preg_replace('#<amp-iframe#', "<div class='amp-iframe-container'><amp-iframe", $html);
        $html = preg_replace('#<\/amp-iframe>#', "</amp-iframe></div>", $html);

        return $html;
    }
}
