<?php

namespace LaravelAMP\Handlers;

use LaravelAMP\Contracts\HandlerContract;

class Image extends Base implements HandlerContract
{
    public function handle(string $html): string
    {
        $html = preg_replace('#<img(.*?)>#', "<div class='amp-img-container'><amp-img data-amp-auto-lightbox-disable $1></amp-img></div>", $html);

        return $html;
    }
}
