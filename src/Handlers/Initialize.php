<?php

namespace LaravelAMP\Handlers;

use LaravelAMP\Contracts\HandlerContract;

class Initialize extends Base implements HandlerContract
{
    private $ampProjectScript = "<script async src=\"https://cdn.ampproject.org/v0.js\"></script>";
    private $ampCarousel = "<script async custom-element=\"amp-carousel\" src=\"https://cdn.ampproject.org/v0/amp-carousel-0.2.js\"></script>";
    private $ampLightBoxGallery = "<script async custom-element=\"amp-lightbox-gallery\" src=\"https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js\"></script>";
    private $ampBind = "<script async custom-element=\"amp-bind\" src=\"https://cdn.ampproject.org/v0/amp-bind-0.1.js\"></script>";
    private $ampBoilerPlate = "<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>";
    private $ampFonts = "<script async custom-element=\"amp-font\" src=\"https://cdn.ampproject.org/v0/amp-font-0.1.js\"></script>";
    private $ampIFrame = "<script async custom-element=\"amp-iframe\" src=\"https://cdn.ampproject.org/v0/amp-iframe-0.1.js\"></script>";
    private $ampSocShare = "<script async custom-element=\"amp-social-share\" src=\"https://cdn.ampproject.org/v0/amp-social-share-0.1.js\"></script>";
    private $ampYouTube = "<script async custom-element=\"amp-youtube\" src=\"https://cdn.ampproject.org/v0/amp-youtube-0.1.js\"></script>";
    private $ampScript = "<script async custom-element=\"amp-script\" src=\"https://cdn.ampproject.org/v0/amp-script-0.1.js\"></script>";
    private $ampAd = "<script async custom-element=\"amp-ad\" src=\"https://cdn.ampproject.org/v0/amp-ad-0.1.js\"></script>";
    private $ampAutoAds = "<script async custom-element=\"amp-auto-ads\" src=\"https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js\"></script>";
    private $ampPosition = "<script async custom-element=\"amp-position-observer\" src=\"https://cdn.ampproject.org/v0/amp-position-observer-0.1.js\"></script>";
    private $ampAnalytics = "<script async custom-element=\"amp-analytics\" src=\"https://cdn.ampproject.org/v0/amp-analytics-0.1.js\"></script>";
    private $ampAutoComplete = "<script async custom-element=\"amp-autocomplete\" src=\"https://cdn.ampproject.org/v0/amp-autocomplete-0.1.js\"></script>";
    private $ampMustache = "<script async custom-template=\"amp-mustache\" src=\"https://cdn.ampproject.org/v0/amp-mustache-0.2.js\"></script>";
    private $ampDate = "<script async custom-element=\"amp-date-picker\" src=\"https://cdn.ampproject.org/v0/amp-date-picker-0.1.js\"></script>";
    private $ampSelector = "<script async custom-element=\"amp-selector\" src=\"https://cdn.ampproject.org/v0/amp-selector-0.1.js\"></script>";
    private $ampList = "<script async custom-element=\"amp-list\" src=\"https://cdn.ampproject.org/v0/amp-list-0.1.js\"></script>";
    private $ampAccordion = "<script async custom-element=\"amp-accordion\" src=\"https://cdn.ampproject.org/v0/amp-accordion-0.1.js\"></script>";

    public function handle(string $html): string
    {
        // always
        $html = $this->includeScript($html, $this->ampBind);

        $html = preg_replace('#<html(.*?)>#', "<html$1 amp>", $html);

        if(preg_match('#amp-carousel#', $html)){
            $html = $this->includeScript($html, $this->ampCarousel);
            $html = $this->includeScript($html, $this->ampLightBoxGallery);
        }

        if(preg_match('#amp-font#', $html)){
            $html = $this->includeScript($html, $this->ampFonts);
        }

        if(preg_match('#amp-iframe#', $html)){
            $html = $this->includeScript($html, $this->ampIFrame);
        }

        if(preg_match('#amp-social-share#', $html)){
            $html = $this->includeScript($html, $this->ampSocShare);
        }

        if(preg_match('#amp-youtube#', $html)){
            $html = $this->includeScript($html, $this->ampYouTube);
        }

        if(preg_match('#amp-script#', $html)){
            $html = $this->includeScript($html, $this->ampScript);
        }

        if(preg_match('#amp-ad#', $html)){
            $html = $this->includeScript($html, $this->ampAd);
        }

        if(preg_match('#amp-auto-ads#', $html)){
            $html = $this->includeScript($html, $this->ampAutoAds);
        }

        if(preg_match('#amp-analytics#', $html)){
            $html = $this->includeScript($html, $this->ampAnalytics);
        }

        if(preg_match('#<amp-autocomplete#', $html)){
            $html = $this->includeScript($html, $this->ampAutoComplete);
        }

        if(preg_match('#amp-mustache#', $html)){
            $html = $this->includeScript($html, $this->ampMustache);
        }

        if(preg_match('#amp-date-picker#', $html)){
            $html = $this->includeScript($html, $this->ampDate);
        }

        if(preg_match('#amp-selector#', $html)){
            $html = $this->includeScript($html, $this->ampSelector);
        }

        if(preg_match('#amp-list#', $html)){
            $html = $this->includeScript($html, $this->ampList);
        }

        if(preg_match('#amp-accordion#', $html)){
            $html = $this->includeScript($html, $this->ampAccordion);
        }

        $html = $this->includeScript($html, $this->ampBoilerPlate);
        $html = $this->includeScript($html, $this->ampProjectScript);
        //$html = $this->includeScript($html, $this->ampPosition);

        return $html;
    }
}
