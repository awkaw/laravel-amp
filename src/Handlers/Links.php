<?php

namespace LaravelAMP\Handlers;

use App\Helpers\LogHelper;
use LaravelAMP\Contracts\HandlerContract;
use LaravelAMP\Services\AmpService;

class Links extends Base implements HandlerContract
{
    public function handle(string $html): string
    {
        $html = preg_replace('#onclick="(.*?)"#', "", $html);
        $html = preg_replace('#href="javascript:(.*?)"#', "", $html);

        if(AmpService::isAmpByRequest()){

            $cHost = explode(":", \request()->getHost())[0];

            $html = preg_replace_callback('#[\s]{1}href="(.*?)"#', function ($matches) use ($cHost){

                if(isset($matches[1])){

                    $link = $matches[1];

                    if(strpos($link, "http") === false || (strpos($link, "http") !== false && strpos($link, "//{$cHost}") !== false)) {

                        if (strpos($link, "?") !== false) {
                            $link = preg_replace('#\?#', '?amp&', $link);
                        } else {
                            $link .= "?amp";
                        }
                    }

                    return " href=\"{$link}\"";
                }

                return $matches[0];

            }, $html);
        }

        return $html;
    }
}
