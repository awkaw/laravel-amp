<?php

namespace LaravelAMP\Handlers;

use App\Helpers\LogHelper;
use LaravelAMP\Contracts\HandlerContract;

class Menu extends Base implements HandlerContract
{
    public function handle(string $html): string
    {

        $html = preg_replace('#<menu>#', "<div class='amp-menu'>", $html);
        $html = preg_replace('#<\/menu>#', "</div>", $html);

        return $html;
    }
}
