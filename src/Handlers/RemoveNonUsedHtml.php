<?php


namespace LaravelAMP\Handlers;


use LaravelAMP\Contracts\HandlerContract;

class RemoveNonUsedHtml extends Base implements HandlerContract
{
    public function handle(string $html): string
    {
        return $html;
    }
}
