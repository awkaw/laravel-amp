<?php

namespace LaravelAMP;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Log;
use LaravelAMP\Services\AmpService;

class ServiceProvider extends \Illuminate\Support\ServiceProvider{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->commands([
            CompileCommand::class,
        ]);*/
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('amp', function () {
            return AmpService::isAmp();
        });

        Blade::if('html', function () {
            return AmpService::isHtml();
        });

        $this->mergeConfigFrom(
            __DIR__.'/../config/amp.php', 'amp'
        );
    }
}
