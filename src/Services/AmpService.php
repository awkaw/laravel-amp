<?php

namespace LaravelAMP\Services;

class AmpService
{
    private static $active = null;

    public static function enable(){
        self::$active = true;
    }

    public static function disable(){
        self::$active = false;
    }

    protected static function getDomain(){

        $domain = "localhost";

        $arrDomain = explode(":", \request()->getHttpHost());

        if(isset($arrDomain[0])) {
            $domain = $arrDomain[0];
        }

        return $domain;
    }

    public static function isAmp(): bool{

        if(self::$active === true){
            return true;
        }

        if(self::$active === false){
            return false;
        }

        if(config("amp.enable_for_all_domains") === true){
            return true;
        }

        return (self::isAmpByDomain() || self::isAmpByRequest());
    }

    public static function isAmpByDomain(): bool{

        $domains = config("amp.domains");

        if(is_array($domains) && !empty($domains) && in_array(self::getDomain(), $domains)){
            return true;
        }

        return false;
    }

    public static function isAmpByRequest(): bool{
        return \request()->has("amp");
    }

    public static function isHtml(): bool{
        return !self::isAmp();
    }
}
