<?php

namespace LaravelAMP\Services;

use LaravelAMP\Contracts\HandlerContract;
use LaravelAMP\Handlers\Image;

class RenderService
{
    public function render($html): string{

        $handlers = config("amp.handlers");

        if(!empty($handlers)){

            foreach ($handlers as $handler) {

                $handlerObj = new $handler();

                if($handlerObj instanceof HandlerContract){
                    $html = $handlerObj->handle($html);
                }
            }
        }

        return $html;
    }
}
